/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.nfcproject.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
  public static final String AUTHORITY = "com.paradox.nfcproject.provider.SimpleContentProvider";
  public static final String DB_NAME = "nfccontentprovider.db";
  public static final int DB_VERSION = 1;

  public interface TaskColumns {
    String ID = BaseColumns._ID;
    String NAME = "name";
    String EXTRAS = "extras";
    String CREATED_ON = "created_on";
  }

  public static final class TaskTable implements TaskColumns {
    public static final String TABLE_NAME = "task";
    public static final String URI_PATH = "task";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + URI_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + URI_PATH;

    private TaskTable() {
    }
  }

  public interface ActionColumns {
    String ID = BaseColumns._ID;
    String TASK_ID = "task_id";
    String TYPE = "type";
    String ACTION = "action";
    String TURN_ON_OFF = "turn_on_off";
    String USERNAME = "username";
    String PASSWORD = "password";
    String DEVICE_NAME = "device_name";
    String DEVICE_MAC = "device_mac";
    String ENCRYPT_TYPE = "encryptation_type";
    String APPLICATION = "application";
    String EXTRAS = "extras";
    String CREATED_ON = "created_on";

  }

  public static final class ActionTable implements ActionColumns {
    public static final String TABLE_NAME = "action";
    public static final String URI_PATH = "action";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + URI_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + URI_PATH;

    private ActionTable() {
    }
  }

}
