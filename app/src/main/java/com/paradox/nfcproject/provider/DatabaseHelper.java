/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.nfcproject.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
  private static final String TAG = DatabaseHelper.class.getSimpleName();

  public DatabaseHelper(Context context) {
    super(context, DatabaseContract.DB_NAME, null, DatabaseContract.DB_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    // player
    db.execSQL("CREATE TABLE " + DatabaseContract.TaskTable.TABLE_NAME + " (" +
        DatabaseContract.TaskTable.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
        DatabaseContract.TaskTable.NAME + " TEXT," +
        DatabaseContract.TaskTable.EXTRAS + " TEXT," +
        DatabaseContract.TaskTable.CREATED_ON + " INTEGER)");


    // GameState
    db.execSQL("CREATE TABLE " + DatabaseContract.ActionTable.TABLE_NAME + " (" +
        DatabaseContract.ActionColumns.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
        DatabaseContract.ActionColumns.ACTION + " TEXT," +
        DatabaseContract.ActionColumns.TASK_ID + " INTEGER," +
        DatabaseContract.ActionColumns.TYPE + " INTEGER," +
        DatabaseContract.ActionColumns.TURN_ON_OFF + " INTEGER," +
        DatabaseContract.ActionColumns.USERNAME + " TEXT," +
        DatabaseContract.ActionColumns.PASSWORD + " TEXT," +
        DatabaseContract.ActionColumns.DEVICE_NAME + " TEXT," +
        DatabaseContract.ActionColumns.DEVICE_MAC + " TEXT," +
        DatabaseContract.ActionColumns.ENCRYPT_TYPE + " INTEGER," +
        DatabaseContract.ActionColumns.APPLICATION + " TEXT," +
        DatabaseContract.ActionColumns.EXTRAS + " TEXT," +
        DatabaseContract.ActionColumns.CREATED_ON + " INTEGER)");

  }


  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TaskTable.TABLE_NAME);
    db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.ActionTable.TABLE_NAME);
   // db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.NoteLabelTable.TABLE_NAME);
    onCreate(db);
  }
}
