package com.paradox.nfcproject.sql;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.model.Task;
import com.paradox.nfcproject.provider.DatabaseContract;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by estebanlopez on 9/15/15.
 */
public class QueryUtils {
    private static final String TAG = QueryUtils.class.getSimpleName();

    public static ArrayList<Task> getTasks(Context context){

        ArrayList<Task> tasks = new ArrayList<Task>();
        Cursor c = context.getContentResolver().query(DatabaseContract.TaskTable.CONTENT_URI, null, null, null, null);

        if (c != null) {


            while (c.moveToNext()) {
                Task task = new Task();
                int id = c.getInt(c.getColumnIndex(DatabaseContract.TaskTable.ID));

                task.setId(id);
                task.setName(c.getString(c.getColumnIndex(DatabaseContract.TaskTable.NAME)));
                task.setExtras(c.getString(c.getColumnIndex(DatabaseContract.TaskTable.EXTRAS)));
                task.setCreated_on(new Date(c.getLong(c.getColumnIndex(DatabaseContract.TaskTable.CREATED_ON))));


                String[] selectionArgs = new String[1];

                selectionArgs[0] = String.valueOf(id);
                String columnQuery  = DatabaseContract.ActionTable.TASK_ID + "=?";

                Cursor cursorActions = context.getContentResolver().query(DatabaseContract.ActionTable.CONTENT_URI, null, columnQuery, selectionArgs,null);

                ArrayList<Action> actions = new ArrayList<Action>();
                if(cursorActions!=null){
                    while (cursorActions.moveToNext()){
                        Action a = getDataFromCursor(cursorActions);
                        actions.add(a);
                    }
                    task.setActions(actions);
                    cursorActions.close();
                }
                tasks.add(task);



            }
            c.close();
        }
        Log.d(TAG, "TASKS: " + tasks.size());

        return tasks;
    }
    public static Task getTaskById(Context context, int ptaskId){

        Task task = null;
        String[] taskSelectionArgs = new String[1];
        taskSelectionArgs[0] = String.valueOf(ptaskId);
        String columnQueryTask  = DatabaseContract.TaskTable.ID + "=?";


        Cursor c = context.getContentResolver().query(DatabaseContract.TaskTable.CONTENT_URI, null, columnQueryTask, taskSelectionArgs, null);

        if (c != null) {
            task  = new Task();
            while (c.moveToNext()) {

                int id = c.getInt(c.getColumnIndex(DatabaseContract.TaskTable.ID));

                task.setId(id);
                task.setName(c.getString(c.getColumnIndex(DatabaseContract.TaskTable.NAME)));
                task.setExtras(c.getString(c.getColumnIndex(DatabaseContract.TaskTable.EXTRAS)));
                task.setCreated_on(new Date(c.getLong(c.getColumnIndex(DatabaseContract.TaskTable.CREATED_ON))));


                String[] selectionArgs = new String[1];

                selectionArgs[0] = String.valueOf(id);
                String columnQuery  = DatabaseContract.ActionTable.TASK_ID + "=?";

                Cursor cursorActions = context.getContentResolver().query(DatabaseContract.ActionTable.CONTENT_URI, null, columnQuery, selectionArgs,null);

                ArrayList<Action> actions = new ArrayList<Action>();
                if(cursorActions!=null){
                    while (cursorActions.moveToNext()){
                        Action a = getDataFromCursor(cursorActions);
                        actions.add(a);
                    }
                    task.setActions(actions);
                    cursorActions.close();
                }



            }
            c.close();
        }

        return task;
    }

    public static Action getActionById(Context context, int pActionId){

        Action a = null;
        String[] taskSelectionArgs = new String[1];
        taskSelectionArgs[0] = String.valueOf(pActionId);
        String columnQueryTask  = DatabaseContract.ActionTable.ID + "=?";


        Cursor c = context.getContentResolver().query(DatabaseContract.ActionTable.CONTENT_URI, null, columnQueryTask, taskSelectionArgs, null);

        if (c != null) {
            while (c.moveToNext()) {
                 a = getDataFromCursor(c);
            }
            c.close();
        }


        return a;
    }
    private static Action getDataFromCursor(Cursor c){
       Action a = new Action();

        a.setId(c.getInt(c.getColumnIndex(DatabaseContract.ActionTable.ID)));
        a.setTaskId(c.getInt(c.getColumnIndex(DatabaseContract.ActionTable.TASK_ID)));
        a.setType(c.getInt(c.getColumnIndex(DatabaseContract.ActionTable.TYPE)));
        a.setAction(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.ACTION)));
        int booltemp = c.getInt(c.getColumnIndex(DatabaseContract.ActionTable.TURN_ON_OFF));
        a.setTurnOnOff((booltemp == 1) ? true : false);
        a.setUsername(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.USERNAME)));
        a.setPassword(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.PASSWORD)));
        a.setDeviceName(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.DEVICE_NAME)));
        a.setDeviceMac(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.DEVICE_MAC)));
        a.setWifiTypeEncrypt(c.getInt(c.getColumnIndex(DatabaseContract.ActionTable.ENCRYPT_TYPE)));
        a.setApplication(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.APPLICATION)));
        a.setExtras(c.getString(c.getColumnIndex(DatabaseContract.ActionTable.EXTRAS)));
        a.setCreated_on(new Date(c.getLong(c.getColumnIndex(DatabaseContract.ActionTable.CREATED_ON))));
        return a;
    }
    public static Task getDataTaskFromCursor(Context context, Cursor c){
        Task task = new Task();
        int id = c.getInt(c.getColumnIndex(DatabaseContract.TaskTable.ID));

        task.setId(id);
        task.setName(c.getString(c.getColumnIndex(DatabaseContract.TaskTable.NAME)));
        task.setExtras(c.getString(c.getColumnIndex(DatabaseContract.TaskTable.EXTRAS)));
        task.setCreated_on(new Date(c.getLong(c.getColumnIndex(DatabaseContract.TaskTable.CREATED_ON))));
        String[] selectionArgs = new String[1];

        selectionArgs[0] = String.valueOf(id);
        String columnQuery  = DatabaseContract.ActionTable.TASK_ID + "=?";

        Cursor cursorActions = context.getContentResolver().query(DatabaseContract.ActionTable.CONTENT_URI, null, columnQuery, selectionArgs,null);

        ArrayList<Action> actions = new ArrayList<Action>();
        if(cursorActions!=null){
            while (cursorActions.moveToNext()){
                Action a = getDataFromCursor(cursorActions);
                actions.add(a);
            }
            task.setActions(actions);
            cursorActions.close();
        }

        return task;
    }

    public static long saveNewAction(Context context, Action action){
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.ActionTable.TASK_ID, action.getTaskId());
        values.put(DatabaseContract.ActionTable.TYPE, action.getType());
        values.put(DatabaseContract.ActionTable.ACTION, action.getAction());
        values.put(DatabaseContract.ActionTable.TURN_ON_OFF, action.isTurnOnOff());
        values.put(DatabaseContract.ActionTable.USERNAME, action.getUsername());
        values.put(DatabaseContract.ActionTable.PASSWORD, action.getPassword());
        values.put(DatabaseContract.ActionTable.DEVICE_NAME, action.getDeviceName());
        values.put(DatabaseContract.ActionTable.DEVICE_MAC, action.getDeviceMac());
        values.put(DatabaseContract.ActionTable.ENCRYPT_TYPE, action.getWifiTypeEncrypt());
        values.put(DatabaseContract.ActionTable.APPLICATION, action.getApplication());
        values.put(DatabaseContract.ActionTable.EXTRAS, action.getExtras());
        values.put(DatabaseContract.ActionTable.CREATED_ON, new Date().getTime());
        long userId = ContentUris.parseId(context.getContentResolver().insert(DatabaseContract.ActionTable.CONTENT_URI, values));

        return userId;
    }
    public static long saveNewTask(Context context, Task task){
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.TaskTable.NAME, task.getName());
        values.put(DatabaseContract.TaskTable.EXTRAS, task.getExtras());
        values.put(DatabaseContract.ActionTable.CREATED_ON, new Date().getTime());
        long userId = ContentUris.parseId(context.getContentResolver().insert(DatabaseContract.TaskTable.CONTENT_URI, values));

        return userId;
    }
    public static void updateTask(Context context, Task task){
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.TaskTable.NAME, task.getName());
        values.put(DatabaseContract.TaskTable.EXTRAS, task.getExtras());

        int result = context.getContentResolver().update(DatabaseContract.TaskTable.CONTENT_URI, values, DatabaseContract.TaskTable.ID + "=?", new String[]{String.valueOf(task.getId())});
        Log.d(TAG, "---DATA TASK UPDATED---Rows "+result );
    }

    public static void deleteTask(Context context, Task task){
      //  int rowsUpdated =   context.getContentResolver().delete(DatabaseContract.TaskTable.CONTENT_URI,null, null);
        int rowsUpdated =   context.getContentResolver().delete(DatabaseContract.TaskTable.CONTENT_URI, DatabaseContract.TaskTable.ID + "=?", new String[]{String.valueOf(task.getId())});
        Log.d(TAG, "---DATA DELETED--- "+ rowsUpdated);
    }



    public static void updateAction(Context context, Action action){
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.ActionTable.TYPE, action.getType());
        values.put(DatabaseContract.ActionTable.ACTION, action.getAction());
        values.put(DatabaseContract.ActionTable.TURN_ON_OFF, action.isTurnOnOff());
        values.put(DatabaseContract.ActionTable.USERNAME, action.getUsername());
        values.put(DatabaseContract.ActionTable.PASSWORD, action.getPassword());
        values.put(DatabaseContract.ActionTable.DEVICE_NAME, action.getDeviceName());
        values.put(DatabaseContract.ActionTable.DEVICE_MAC, action.getDeviceMac());
        values.put(DatabaseContract.ActionTable.ENCRYPT_TYPE, action.getWifiTypeEncrypt());
        values.put(DatabaseContract.ActionTable.APPLICATION, action.getApplication());
        values.put(DatabaseContract.ActionTable.EXTRAS, action.getExtras());


        int result = context.getContentResolver().update(DatabaseContract.ActionTable.CONTENT_URI, values, DatabaseContract.ActionTable.ID + "=?", new String[]{String.valueOf(action.getId())});
        Log.d(TAG, "---DATA TASK UPDATED---Rows " + result);

       /* int result = context.getContentResolver().update(DatabaseContract.ActionTable.CONTENT_URI, values, DatabaseContract.ActionTable.ID + "=?", new String[]{String.valueOf(action.getId())});
        Log.d(TAG, "---DATA ACTION UPDATED---Rows "+result );*/
    }
    public static void deleteAction(Context context, Action action){
        int rowsDeleted =   context.getContentResolver().delete(DatabaseContract.ActionTable.CONTENT_URI, DatabaseContract.ActionTable.ID + "=?", new String[]{String.valueOf(action.getId())});
        Log.d(TAG, "---DATA DELETED ACTION--- "+ rowsDeleted);
    }

}

