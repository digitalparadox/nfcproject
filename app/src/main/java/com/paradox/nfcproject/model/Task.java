package com.paradox.nfcproject.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by estebanlopez on 9/14/15.
 */
public class Task {

    private int id;
    private ArrayList<Action> actions;
    private String name ="";
    private String extras ="";
    private Date created_on;

    public Task() {
        actions = new ArrayList<Action>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Action> getActions() {
        return actions;
    }

    public void setActions(ArrayList<Action> actions) {
        this.actions = actions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }


}
