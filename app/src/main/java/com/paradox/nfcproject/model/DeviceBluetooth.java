package com.paradox.nfcproject.model;

/**
 * Created by estebanlopeza on 16/9/15.
 */
public class DeviceBluetooth {

    private String name;
    private String mac;

    public DeviceBluetooth() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
