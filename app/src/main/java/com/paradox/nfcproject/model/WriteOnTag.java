package com.paradox.nfcproject.model;

/**
 * Created by estebanlopez on 10/4/15.
 */
public interface WriteOnTag {
    boolean writeIt(Task task);
}
