package com.paradox.nfcproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.paradox.nfcproject.R;
import com.paradox.nfcproject.model.DeviceBluetooth;

import java.util.ArrayList;

/**
 * Created by estebanlopeza on 16/9/15.
 */
public class BluetoothListAdapter extends ArrayAdapter<DeviceBluetooth> {

    ArrayList<DeviceBluetooth> devices;

    public BluetoothListAdapter(Context context, ArrayList<DeviceBluetooth> devices) {
        super(context, 0, devices);
        this.devices = devices;
    }
    public ArrayList<DeviceBluetooth> getDevices(){
        return devices;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DeviceBluetooth bluetoothDevice = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.bluetooth_item_row, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvNameBluetooth);
        TextView tvHome = (TextView) convertView.findViewById(R.id.tvMacBluetooth);
        // Populate the data into the template view using the data object
        tvName.setText(bluetoothDevice.getName());
        tvHome.setText(bluetoothDevice.getMac());
        // Return the completed view to render on screen
        return convertView;
    }
}