package com.paradox.nfcproject.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.model.Action;

import java.util.ArrayList;

/**
 * Created by estebanlopez on 9/15/15.
 */
public class ActionsAdapter extends RecyclerView.Adapter<ActionsAdapter.ViewHolder> {

    private ArrayList<Action> mActions;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvActionName, tvActionType, tvActionDescription;
        public ViewHolder(View v) {
            super(v);
            tvActionName = (TextView) itemView.findViewById(R.id.tvActionName);
            tvActionType = (TextView) itemView.findViewById(R.id.tvActionType);
            tvActionDescription = (TextView) itemView.findViewById(R.id.tvActionDescription);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ActionsAdapter(ArrayList<Action> pactions) {
        mActions = pactions;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ActionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_action, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvActionName.setText(mActions.get(position).getAction());
        holder.tvActionType.setText(mActions.get(position).isTurnOnOff()?"ON":"OFF");

      //  holder.tvActionType.setText(App.getType(mActions.get(position).getType()));
        String description = mActions.get(position).getDeviceName();
        if(mActions.get(position).getType() == App.TYPE_LAUNCH_APP){
            description = mActions.get(position).getApplication();
            holder.tvActionType.setText("LAUNCH");
        }
        holder.tvActionDescription.setText(description);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mActions.size();
    }



}
