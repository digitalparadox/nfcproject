package com.paradox.nfcproject.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paradox.nfcproject.R;
import com.paradox.nfcproject.activity.TaskActionsActivity;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.model.Task;
import com.paradox.nfcproject.model.WriteOnTag;
import com.paradox.nfcproject.provider.DatabaseContract;
import com.paradox.nfcproject.sql.QueryUtils;

import java.util.ArrayList;

/**
 * Created by estebanlopez on 9/18/15.
 */
public class MyTaskListCursorAdapter extends CursorRecyclerViewAdapter<MyTaskListCursorAdapter.ViewHolder> {
    Context mContext;

    Activity activity;
    public MyTaskListCursorAdapter(Context context,Cursor cursor, Activity pac){
        super(context,cursor);
        mContext = context;
        activity = pac;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvDescription;
        public Button bWriteTag, bDeleteTask;
        public LinearLayout llContainerCard;
        public ViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            bWriteTag = (Button) itemView.findViewById(R.id.bWriteTag);
            bDeleteTask = (Button) itemView.findViewById(R.id.bDeleteTask);
            llContainerCard = (LinearLayout) itemView.findViewById(R.id.llContainerCard);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_task, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        final Task task = QueryUtils.getDataTaskFromCursor(mContext, cursor);
        viewHolder.tvName.setText(task.getName());

        String desc = "";
        ArrayList<Action> actions = task.getActions();
        for(Action tempAction: actions){
            desc += tempAction.getAction() + " > ";
        }
        viewHolder.tvDescription.setText(desc);

        viewHolder.llContainerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int task_id = task.getId();
                Intent intent = new Intent(activity, TaskActionsActivity.class);
                intent.putExtra("task_id", task_id);
                activity.startActivityForResult(intent, 1);
                //  Toast.makeText(getApplicationContext(), "Position "+position, Toast.LENGTH_LONG).show();

            }
        });

        viewHolder.bDeleteTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryUtils.deleteTask(mContext, task);

                changeCursor(mContext.getContentResolver().query(DatabaseContract.TaskTable.CONTENT_URI, null, null, null, null));
            }
        });
        viewHolder.bWriteTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    ((WriteOnTag) activity).writeIt(task);
                }catch (ClassCastException cce){

                }
            }
        });
    }
}
