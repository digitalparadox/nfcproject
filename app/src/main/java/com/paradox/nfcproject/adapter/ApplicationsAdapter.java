package com.paradox.nfcproject.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.paradox.nfcproject.R;

import java.util.List;

public class ApplicationsAdapter extends ArrayAdapter<ApplicationInfo> {


        Context context;
    private PackageManager packageManager;
    private List<ApplicationInfo> appsList = null;


        public ApplicationsAdapter(Context context, int resourceId, //resourceId=your layout
        List<ApplicationInfo> items) {
            super(context, resourceId, items);
            this.context = context;
            this.appsList = items;

            packageManager = context.getPackageManager();
        }

/*private view holder class*/
        private class ViewHolder {
            ImageView imageView;
            TextView txtTitle;
            TextView tvAppPackageName;
        }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;


        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.application_item_row, null);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.tvAppName);
            holder.tvAppPackageName = (TextView) convertView.findViewById(R.id.tvAppPackageName);
            holder.imageView = (ImageView) convertView.findViewById(R.id.ivAppIcon);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        ApplicationInfo data = getItem(position);
        if (null != data) {


            holder.txtTitle.setText(data.loadLabel(packageManager));
            holder.tvAppPackageName.setText(data.packageName);
            holder.imageView.setImageDrawable(data.loadIcon(packageManager));
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return ((null != appsList) ? appsList.size() : 0);
    }

    @Override
    public ApplicationInfo getItem(int position) {
        return ((null != appsList) ? appsList.get(position) : null);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
