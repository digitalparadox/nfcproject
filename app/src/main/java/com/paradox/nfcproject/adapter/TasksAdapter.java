package com.paradox.nfcproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.paradox.nfcproject.R;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.model.Task;
import com.paradox.nfcproject.sql.QueryUtils;

import java.util.ArrayList;

/**
 * Created by estebanlopez on 9/15/15.
 */
public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {

    private ArrayList<Task> mTasks;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvName, tvDescription;
        public Button bWriteTag, bDeleteTask;
        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            bWriteTag = (Button) itemView.findViewById(R.id.bWriteTag);
            bDeleteTask = (Button) itemView.findViewById(R.id.bDeleteTask);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TasksAdapter(ArrayList<Task> ptasks, Context pcontext) {
        mTasks = ptasks;
        context = pcontext;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TasksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_task, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvName.setText(mTasks.get(position).getName());

        String desc = "";
        ArrayList<Action> actions = mTasks.get(position).getActions();
        for(Action tempAction: actions){
            desc += tempAction.getAction() + " > ";
        }
        holder.tvDescription.setText(desc);

        holder.bDeleteTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryUtils.deleteTask(context, mTasks.get(position));
            }
        });
        holder.bWriteTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
    public ArrayList<Task> getTasks(){
        return mTasks;
    }
    public Task getTaskByPosition(int position){
        return mTasks.get(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mTasks.size();
    }


}
