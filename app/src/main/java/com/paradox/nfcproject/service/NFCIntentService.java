package com.paradox.nfcproject.service;

import android.app.IntentService;
import android.content.Intent;

import com.paradox.nfcproject.util.Interpreter;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class NFCIntentService extends IntentService {

    public static final String ACTION_PROGRESO =
            "com.paradox.intent.action.ACTION_INTERPRETER";
    public static final String ACTION_FIN =
            "net.sgoliver.intent.action.FIN_INTERPRETER";

    public NFCIntentService() {
        super("NFCIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        String tag_string = intent.getStringExtra("tag_string");

        Interpreter in = new Interpreter(getBaseContext());
        in.resolveAndExecuteString(tag_string);

    }


}