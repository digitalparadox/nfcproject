package com.paradox.nfcproject.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.paradox.nfcproject.App;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.model.Task;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;


public class Interpreter {

    public static final int TYPE_WIFI = 1;
    public static final int TYPE_BLUETOOTH = 2;
    public static final int TYPE_GPS = 3;
    public static final int TYPE_SHARE_CARD = 4;
    public static final int TYPE_LAUNCH_APP = 5;


    Context context;

    public Interpreter(Context pcontx) {
        context = pcontx;
    }

    public void format(Task task, Tag tag) {


        String result = "";
        StringBuilder builder = new StringBuilder();
        builder.append("#");


        for (Action action : task.getActions()) {
            builder
                    .append(action.getType()).append("|")
                    .append(boolToInt(action.isTurnOnOff())).append("|")
                    .append(getActionString(action.getDeviceName())).append("|")
                    .append(getActionString(action.getDeviceMac())).append("|")
                    .append(getActionString(action.getPassword())).append("|")
                    .append(action.getWifiTypeEncrypt()).append("|")
                    .append(getActionString(action.getExtras())).append("#");

        }

        if (task.getActions() != null && task.getActions().size() > 0) {
            result = builder.toString();
            writeTag(result, tag);
        } else {
            Toast.makeText(context, "No hay acciones para escribir", Toast.LENGTH_LONG).show();
        }

    }

    private String getActionString(String value) {
        return (!value.isEmpty()) ? value : "none";
    }

    public String getPayloadFromTag(Tag tag) {
        String result = "";
        return result;
    }

    public void resolveAndExecuteString(String byteCode) {

        //   List<String> actionList = new ArrayList<String>();

        String[] actionsArray = byteCode.split("#");

        for (int i = 0; i < actionsArray.length; i++) {
            //  actionList.add(actionsArray[i]);
            if (!actionsArray[i].equals(""))
                executeActionStringAndExecute(actionsArray[i]);
        }
    }

    private void executeActionStringAndExecute(String paction) {
        String[] values = paction.split("\\|");
        int type = Integer.parseInt(values[0]);
        // boolean turnOnOff = intToBool(Integer.parseInt(values[1]));
        int turnOnOff = Integer.parseInt(values[1]);
        String deviceName = values[2];
        String deviceMac = values[3];
        String password = values[4];
        int wifiTypeEncript = Integer.parseInt(values[5]);
        String extras = values[6];

        switch (type) {
            case TYPE_WIFI:
                boolean status = intToBool(turnOnOff);
                enableDisableWifi(status, deviceName, password, wifiTypeEncript);
                break;
            case TYPE_BLUETOOTH:
                if (turnOnOff == 0) {
                    App.setBluetooth(false);
                } else {
                    App.setBluetooth(true);
                    if (!deviceName.equalsIgnoreCase("none")) {
                        connectDeviceBluetooth(true, deviceName, deviceMac);
                    }
                }
                break;
            case TYPE_GPS:

                if (turnOnOff == 0) {
                    turnGPSOff();
                } else {
                    final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        turnGPSOn();
                    }

                }
                break;
            case TYPE_SHARE_CARD:
                //  if(turnOnOff == 0){
                shareContact();
                // }else{

                //  }
                break;
            case TYPE_LAUNCH_APP:
                //   if(turnOnOff == 1){
                launchApplication(extras);
           /*     }else{

                }*/
                break;
            default:
                break;
        }

    }

    private void launchApplication(String extras) {
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(extras);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(launchIntent);
    }

    private void shareContact() {


    }


    public static int boolToInt(boolean bool) {
        return (bool) ? 1 : 0;
    }

    public static boolean intToBool(int value) {
        return (value == 0) ? false : true;
    }


    public static String getType(int type) {
        switch (type) {
            case TYPE_WIFI:
                return "WIFI";
            case TYPE_BLUETOOTH:
                return "BLUETOOTH";
            case TYPE_GPS:
                return "GPS";
            case TYPE_SHARE_CARD:
                return "SHARE_CARD";
            case TYPE_LAUNCH_APP:
                return "LAUNCH_APP";
            default:
                return "";
        }
    }

    public void connectDeviceBluetooth(boolean enable, String deviceName, String deviceMac) {


        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
/*
        ArrayList<DeviceBluetooth> devices = new ArrayList<DeviceBluetooth>();
        for(BluetoothDevice bt : pairedDevices) {
            DeviceBluetooth deviceBluetooth = new DeviceBluetooth();
            deviceBluetooth.setName(bt.getName());
            deviceBluetooth.setMac(bt.getAddress());
            devices.add(deviceBluetooth);
        }*/
        BluetoothDevice result = null;

        if (pairedDevices != null) {
            for (BluetoothDevice device : pairedDevices) {
                if (deviceMac.equals(device.getAddress())) {
                    result = device;
                    break;
                }
            }
        }
        if (result != null) {
            Method m = null;
            try {
                m = result.getClass().getMethod("createRfcommSocket",
                        new Class[]{int.class});

            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            try {
                BluetoothSocket mySocket = (BluetoothSocket) m.invoke(result, Integer.valueOf(1));

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

    }

    public void turnGPSOn() {
        connectGoogleApi();

        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    GoogleApiClient mGoogleApiClient = null;
    LocationRequest request;

    public void connectGoogleApi() {
       /*   mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();

                        request=new LocationRequest()
                                .setNumUpdates(1)
                                .setExpirationDuration(60000)
                                .setInterval(1000)
                                .setPriority(LocationRequest.PRIORITY_LOW_POWER);
                        LocationSettingsRequest.Builder b=
                                new LocationSettingsRequest.Builder()
                                        .addLocationRequest(request);
                        PendingResult<LocationSettingsResult> result =
                                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                                        b.build());

                        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                            @Override
                            public void onResult(LocationSettingsResult locationSettingsResult) {
                                Toast.makeText(context, "locationSettingsResult "+locationSettingsResult.getStatus(), Toast.LENGTH_SHORT).show();

                            }
                        });



                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Toast.makeText(context, "Suspended", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Toast.makeText(context, "FAILED", Toast.LENGTH_SHORT).show();
                    }
                })
                .build();

        mGoogleApiClient.connect();*/

    }


    // automatic turn off the gps
    public void turnGPSOff() {
        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }


    private void enableDisableWifi(boolean status, String deviceName, String password, int typeEncrypt) {
/*
        WifiManager wifiManager = (WifiManager)this.context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(status);
        */

        int networkId = -1;

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(status);
        // setup a wifi configuration
        WifiConfiguration wc = null;
        if (status == true && !deviceName.equalsIgnoreCase("none")) {
            wc = new WifiConfiguration();

         /*   wc.SSID = "\"" + deviceName + "\"";
            if (!password.equalsIgnoreCase("none")) {
                wc.preSharedKey = "\"" + password + "\"";
            }*/


            wc.SSID = String.format("\"%s\"", deviceName);
            if (!password.equalsIgnoreCase("none")) {
                wc.preSharedKey = String.format("\"%s\"", password);
            }

            //   wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

            wc.status = WifiConfiguration.Status.ENABLED;
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wc.allowedKeyManagement.set(typeEncrypt);
            wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

            networkId = wifiManager.addNetwork(wc);
        }
        wifiManager.saveConfiguration();


// connect to and enable the connection
        if (wc != null) {
           /* int netId = wifiManager.addNetwork(wc);
            wifiManager.enableNetwork(netId, true);
            wifiManager.reconnect();


*/
            if (networkId != -1) {
                // success, can call wfMgr.enableNetwork(networkId, true) to connect

                wifiManager.disconnect();
                wifiManager.enableNetwork(networkId, true);
                wifiManager.reconnect();
            }
           /* List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration i : list) {
                WifiConfiguration wfCo = i;

                if (i.SSID != null && i.SSID.equalsIgnoreCase("\"" + deviceName + "\"")) {
             //   if (i.SSID != null && i.SSID.equals("\"" + deviceName + "\"")) {
                    wifiManager.disconnect();
                    wifiManager.enableNetwork(i.networkId, true);
                    wifiManager.reconnect();

                    break;
                }
            }
*/
        }





    }

    private void enableDisableWifiTest2(boolean status, String deviceName, String password, int typeEncrypt) {

    }

    NfcAdapter adapter;


    public void writeTag(String row, Tag myTag) {
        try {
            //Si no existe tag al que escribir, mostramos un mensaje de que no existe.
            if (myTag == null) {
                Toast.makeText(context, "Error: no hay ninguna tarjeta.", Toast.LENGTH_LONG).show();
            } else {
                //Llamamos al método write que definimos más adelante donde le pasamos por
                //parámetro el tag que hemos detectado y el mensaje a escribir.
                write(row, myTag);
                Toast.makeText(context, "Se ha escrito en la tarjeta", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            Toast.makeText(context, "Error Write", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (FormatException e) {
            Toast.makeText(context, "Error format", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    //El método write es el más importante, será el que se encargue de crear el mensaje
    //y escribirlo en nuestro tag.
    private void write(String text, Tag tag) throws IOException, FormatException {
        //Creamos un array de elementos NdefRecord. Este Objeto representa un registro del mensaje NDEF
        //Para crear el objeto NdefRecord usamos el método createRecord(String s)
        NdefRecord[] records = {createRecord(text)};
        //  NdefRecord[] records = {createRecord(text)};
        //NdefMessage encapsula un mensaje Ndef(NFC Data Exchange Format). Estos mensajes están
        //compuestos por varios registros encapsulados por la clase NdefRecord
        //  NdefMessage message = new NdefMessage(records);
        //Obtenemos una instancia de Ndef del Tag

        NdefMessage message2 = createNdefMessage(text);
        Ndef ndef = Ndef.get(tag);
        ndef.connect();
        ndef.writeNdefMessage(message2);
        ndef.close();

    }

    private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
        String lang = "us";
        byte[] textBytes = text.getBytes();
        byte[] langBytes = lang.getBytes("US-ASCII");
        int langLength = langBytes.length;
        int textLength = textBytes.length;
        byte[] payLoad = new byte[1 + langLength + textLength];

        payLoad[0] = (byte) langLength;

        System.arraycopy(langBytes, 0, payLoad, 1, langLength);
        System.arraycopy(textBytes, 0, payLoad, 1 + langLength, textLength);

        //   NdefRecord record = NdefRecord.createMime( "text/html", "Hello world in <b>HTML</b> !");

        NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payLoad);

        return recordNFC;

    }

    public NdefMessage createNdefMessage(String text) {

        NdefMessage msg = new NdefMessage(
                new NdefRecord[]{NdefRecord.createMime(
                        "application/" + context.getPackageName(), text.getBytes())
                        , NdefRecord.createApplicationRecord(context.getPackageName())
                });
        return msg;
    }

}
