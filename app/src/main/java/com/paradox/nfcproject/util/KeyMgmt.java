package com.paradox.nfcproject.util;


public class KeyMgmt {
    private KeyMgmt() { }

    /** WPA is not used; plaintext or static WEP could be used. */
    public static final int NONE = 0;
    /** WPA pre-shared key (requires {@code preSharedKey} to be specified). */
    public static final int WPA_PSK = 1;
    /** WPA using EAP authentication. Generally used with an external authentication server. */
    public static final int WPA_EAP = 2;
    /** IEEE 802.1X using EAP authentication and (optionally) dynamically
     * generated WEP keys. */
    public static final int IEEE8021X = 3;

    /** WPA2 pre-shared key for use with soft access point
     * (requires {@code preSharedKey} to be specified).
     * @hide
     */
    public static final int WPA2_PSK = 4;

    public static final String varName = "key_mgmt";

    public static final String[] strings = { "NONE", "WPA_PSK", "WPA_EAP", "IEEE8021X",
            "WPA2_PSK" };
}
