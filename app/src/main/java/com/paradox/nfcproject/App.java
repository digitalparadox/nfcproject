package com.paradox.nfcproject;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;

import com.paradox.nfcproject.provider.DatabaseContract;

import java.util.Date;

/**
 * Created by estebanlopez on 9/14/15.
 */

public class App extends Application {

    public static final int TYPE_WIFI = 1;
    public static final int TYPE_BLUETOOTH = 2;
    public static final int TYPE_GPS = 3;
    public static final int TYPE_SHARE_CARD = 4;
    public static final int TYPE_LAUNCH_APP = 5;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();


    }

  

    private void insertFirstDataRule(String pdescription){
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.ActionTable.ACTION, pdescription);
        values.put(DatabaseContract.ActionTable.CREATED_ON, new Date().getTime());
        long noteId1 = ContentUris.parseId(getContentResolver().insert(DatabaseContract.ActionTable.CONTENT_URI, values));
    }

    public static boolean setBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }
        // No need to change bluetooth state
        return true;
    }
    public static boolean isBluetoothEnable(){
        //Disable bluetooth
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
           return true;
        }else{
            return false;
        }


    }
    public static String getType(int type){
        switch (type){
            case TYPE_WIFI:
                return "WIFI";
            case TYPE_BLUETOOTH:
                return "BLUETOOTH";
            case TYPE_GPS:
                return "GPS";
            case TYPE_SHARE_CARD:
                return "SHARE_CARD";
            case TYPE_LAUNCH_APP:
                return "LAUNCH_APP";
            default:
                return "";
        }
    }
}
