package com.paradox.nfcproject.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paradox.nfcproject.R;
import com.paradox.nfcproject.adapter.MyTaskListCursorAdapter;
import com.paradox.nfcproject.model.Task;
import com.paradox.nfcproject.model.WriteOnTag;
import com.paradox.nfcproject.provider.DatabaseContract;
import com.paradox.nfcproject.sql.QueryUtils;
import com.paradox.nfcproject.util.Interpreter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements WriteOnTag {

    private Toolbar toolbar;
    private RecyclerView mRecyclerViewTasks;
    private MyTaskListCursorAdapter mAdapter;
 //   private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<Task> tasks;

    private FloatingActionButton fabAddTask;
    private int NEW_TASK = 1;


    PackageManager pm;

    //nfc

    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    boolean writeMode;
    NfcAdapter adapter;
    Tag myTag;

    TextView tvTagType, tvTagStorage, tvTagWritable;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();


        checkAndInit();
        initNFC();
    }

    private void checkAndInit(){
         pm = this.getPackageManager();
        // Check whether NFC is available on device
        if (!pm.hasSystemFeature(PackageManager.FEATURE_NFC)) {
            // NFC is not available on the device.
            Toast.makeText(this, "The device does not has NFC hardware.",
                    Toast.LENGTH_SHORT).show();
        }
        // Check whether device is running Android 4.1 or higher
        else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            // Android Beam feature is not supported.
         //   Toast.makeText(this, "Android Beam is not supported.", Toast.LENGTH_SHORT).show();
        }
        else {
            // NFC and Android Beam file transfer is supported.
        //    Toast.makeText(this, "Android Beam is supported on your device.", Toast.LENGTH_SHORT).show();
        }
    }

    private void initNFC() {
        adapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};
    }



    private void initComponents() {

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        tvTagType = (TextView) findViewById(R.id.tvTagType);
        tvTagStorage = (TextView) findViewById(R.id.tvTagStorage);
        tvTagWritable = (TextView) findViewById(R.id.tvTagWritable);



        mRecyclerViewTasks = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerViewTasks.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewTasks.setLayoutManager(mLayoutManager);

       /* mRecyclerViewTasks.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // ...
                int task_id = tasks.get(position).getId();
                Intent intent = new Intent(MainActivity.this, TaskActionsActivity.class);
                intent.putExtra("task_id", task_id);
                startActivityForResult(intent, NEW_TASK);
                //  Toast.makeText(getApplicationContext(), "Position "+position, Toast.LENGTH_LONG).show();
            }

        }));*/


        fabAddTask = (FloatingActionButton)findViewById(R.id.fabAddTask);
        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TaskActionsActivity.class));
            }
        });

        testData();


    }

    private void testData() {
       // Interpreter i = new Interpreter(getApplicationContext());
       // i.resolveAndExecuteString("#3|0|none|none|none|0|none");
     //   i.resolveAndExecuteString("#1|1|Losmasmas|none|diaest27|4|none#");
    }

    private void loadData() {
        tasks = QueryUtils.getTasks(this);
        Cursor c = getApplicationContext().getContentResolver().query(DatabaseContract.TaskTable.CONTENT_URI, null, null, null, null);
        mAdapter = new MyTaskListCursorAdapter(getApplicationContext(),c, MainActivity.this);
        mRecyclerViewTasks.setAdapter(mAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_info_tag) {
            return true;
        }

        if (id == R.id.action_clear_tag) {


            try {
                if(myTag!=null) {
                    Ndef ndefTag = Ndef.get(myTag);
                    ndefTag.connect();
                    ndefTag.writeNdefMessage(new NdefMessage(new NdefRecord(NdefRecord.TNF_EMPTY, null, null, null)));
                    ndefTag.close();
                    Toast.makeText(getApplicationContext(), "Tag Deleted", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FormatException e) {
                e.printStackTrace();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause(){
        super.onPause();
        WriteModeOff();
    }
    @Override
    public void onResume(){
        super.onResume();
        WriteModeOn();
        loadData();
    }
    private void WriteModeOn(){
        writeMode = true;
       adapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    private void WriteModeOff(){
        writeMode = false;
        adapter.disableForegroundDispatch(this);
    }
    //en onnewIntent manejamos el intent para encontrar el Tag
    protected void onNewIntent(Intent intent) {

        String action = intent.getAction();

        String type2 = intent.getType();
      //  if (intent.getType() != null && intent.getType().equals("application/" + getPackageName())) {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

                Toast.makeText(this, "Detected ", Toast.LENGTH_LONG).show();//myTag.toString()


                // get NDEF tag details
                Ndef ndefTag = Ndef.get(myTag);
                displayDataTag(ndefTag);


                Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

                //execute
                if(rawMsgs!=null && rawMsgs.length>0){
                    NdefRecord relayRecord = ((NdefMessage)rawMsgs[0]).getRecords()[0];
                    String nfcData = new String(relayRecord.getPayload());
                    if (nfcData != null) {
                        //  resolveTagString(nfcData);
                    }
                }



                //   if (MIME_TEXT_PLAIN.equals(type)) {

                //  Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
             //    new NdefReaderTask().execute(myTag);


            } else {
                //Log.d("MainActivity", "Wrong mime type: " + type);
            }


       // }
    }
    private void displayDataTag( Ndef ndefTag){
        int size = ndefTag.getMaxSize();         // tag size
        boolean writable = ndefTag.isWritable(); // is tag writable?
        String type = ndefTag.getType();         // tag type
        tvTagType.setText(type);
        tvTagStorage.setText(size + "");
        tvTagWritable.setText((writable) ? "Writable: YES" : "Writable: NO");
    }


    public void resolveTagString(String pstring){
        if(!pstring.isEmpty()){
            Interpreter in = new Interpreter(this);
            in.resolveAndExecuteString(pstring);
        }else{
            Toast.makeText(this, "Empty TAG" , Toast.LENGTH_LONG).show();//myTag.toString()

        }

    }
    @Override
    public boolean writeIt(Task task) {
        boolean result = false;


      //  if(myTag!=null){
            Interpreter interpreter = new Interpreter(this);
            interpreter.format(task, myTag);
    //    }

        return result;
    }

    /**
     * Background task for reading the data. Do not block the UI thread while reading.
     *
     * @author Ralf Wondratschek
     *
     */
     class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e("MainActivity", "Unsupported Encoding", e);
                    }
                }
            }

            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */

            byte[] payload = record.getPayload();

            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                resolveTagString(result);
            }
        }
    }
}
