package com.paradox.nfcproject.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.sql.QueryUtils;

public class GPSActionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FloatingActionButton fabAddTask;
    private ImageView ivGPSOnOff;

    private SwitchCompat sEnableDisableGPS;


    private boolean enableDisableGPS = false;
    Action action;

    int taskId = 0;
    int actionId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpsaction);

        initComponents();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskId = extras.getInt("task_id");
            actionId = extras.getInt("action_id", 0);
            if (actionId > 0) {
                action = QueryUtils.getActionById(getApplicationContext(), actionId);
                if (action != null) {
                    loadData(action);
                }
            }


        }


    }


    private void initComponents() {

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setElevation(0);

        getSupportActionBar().setTitle("");



        sEnableDisableGPS = (SwitchCompat) findViewById(R.id.sEnableDisableGPS);
        ivGPSOnOff = (ImageView) findViewById(R.id.ivGPSOnOff);

        fabAddTask = (FloatingActionButton) findViewById(R.id.fabAddActionGPS);
        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveActionData();
            }
        });

        sEnableDisableGPS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    enableDisableGPS = true;
                    ivGPSOnOff.setImageResource(R.drawable.gps_on);
                } else {
                    enableDisableGPS = false;
                    ivGPSOnOff.setImageResource(R.drawable.gps_of);
                }

            }
        });
        if(action == null){
            ivGPSOnOff.setImageResource(R.drawable.gps_of);

        }
        ivGPSOnOff.setColorFilter(Color.GRAY);
    }

    private void saveActionData() {

        if(action == null)
            action = new Action();


        action.setTaskId(taskId);
        action.setType(App.TYPE_GPS);
        action.setAction("GPS");
        action.setTurnOnOff(enableDisableGPS);


        long id = 0;
        if(actionId>0){
            QueryUtils.updateAction(getApplicationContext(), action);
        }else{
            id=QueryUtils.saveNewAction(getApplicationContext(), action);
        }


        Intent i = getIntent();
        i.putExtra("RESULT", id);
        setResult(RESULT_OK, i);
        finish();

    }

    private void loadData(Action action) {
        sEnableDisableGPS.setChecked(action.isTurnOnOff());
        if(action.isTurnOnOff()){
            ivGPSOnOff.setImageResource(R.drawable.gps_on);
        }else{
            ivGPSOnOff.setImageResource(R.drawable.gps_of);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
