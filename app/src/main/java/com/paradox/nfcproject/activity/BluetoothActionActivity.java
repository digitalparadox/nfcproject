package com.paradox.nfcproject.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.adapter.BluetoothListAdapter;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.model.DeviceBluetooth;
import com.paradox.nfcproject.sql.QueryUtils;

import java.util.ArrayList;
import java.util.Set;

public class BluetoothActionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FloatingActionButton fabAddTask;

    private SwitchCompat sEnableDisableBluetooth;
    private ListView lvBluetoothDevices;


    private boolean enableDisableBluetooth =false;
    private DeviceBluetooth selectedDeviceBluetooth = null;
    BluetoothListAdapter itemsAdapter;
    Action action;

    int taskId = 0;
    int actionId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_action);

        initComponents();
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            taskId = extras.getInt("task_id");
            actionId = extras.getInt("action_id", 0);
            if(actionId>0){
                action = QueryUtils.getActionById(getApplicationContext(), actionId);
                if(action!=null){
                    loadData(action);
                }
            }



        }


    }


    private void initComponents() {

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setElevation(0);

        getSupportActionBar().setTitle("");

        sEnableDisableBluetooth = (SwitchCompat) findViewById(R.id.sEnableDisableBluetooth);
        lvBluetoothDevices = (ListView)findViewById(R.id.lvBluetoothDevices);




        fabAddTask = (FloatingActionButton)findViewById(R.id.fabAddTask);
        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveActionData();
            }
        });

        sEnableDisableBluetooth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    enableDisableBluetooth = true;
                    showBluetoothDevices();
                } else {
                    enableDisableBluetooth = false;
                    hideBluetoothDevices();
                }

            }
        });


    }

    private void saveActionData() {

        if(action == null)
           action = new Action();


        action.setTaskId(taskId);
        action.setType(App.TYPE_BLUETOOTH);
        action.setAction("Bluetooth");
        action.setWifiTypeEncrypt(0);
        action.setTurnOnOff(enableDisableBluetooth);
        if(selectedDeviceBluetooth!=null){
            action.setDeviceName(selectedDeviceBluetooth.getName());
            action.setDeviceMac(selectedDeviceBluetooth.getMac());
        }
      //  long id =QueryUtils.saveNewAction(getApplicationContext(), action);


        long id = 0;
        if(actionId>0){
            QueryUtils.updateAction(getApplicationContext(), action);
        }else{
            id=QueryUtils.saveNewAction(getApplicationContext(), action);
        }

        Intent i = getIntent();
        i.putExtra("RESULT", id);
        setResult(RESULT_OK, i);
        finish();

    }

    private void showBluetoothDevices(){

        if(!App.isBluetoothEnable()){
            App.setBluetooth(true);
        }else{
            getDataFromActiveBluetooth();
        }

    }
    private void getDataFromActiveBluetooth(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        ArrayList<DeviceBluetooth> devices = new ArrayList<DeviceBluetooth>();
        for(BluetoothDevice bt : pairedDevices) {
            DeviceBluetooth deviceBluetooth = new DeviceBluetooth();
            deviceBluetooth.setName(bt.getName());
            deviceBluetooth.setMac(bt.getAddress());
            devices.add(deviceBluetooth);
        }
        itemsAdapter = new BluetoothListAdapter(this, devices);
        lvBluetoothDevices.setAdapter(itemsAdapter);


        lvBluetoothDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
              //  view.setSelected(true);
                selectedDeviceBluetooth = itemsAdapter.getDevices().get(position);
            }
        });

        if(action!=null){
            if(action.isTurnOnOff()){
                if(!action.getDeviceName().isEmpty() && !action.getDeviceMac().isEmpty()){
                    int indexDevice = 0;
                    boolean found = false;
                    for(DeviceBluetooth dev :devices){
                        if(dev.getMac().equals(action.getDeviceMac())){
                            selectedDeviceBluetooth = dev;
                            found = true;
                            break;
                        }
                        indexDevice++;
                    }
                    if(found)
                        lvBluetoothDevices.setSelection(indexDevice);

                }
            }
        }

    }
    private void hideBluetoothDevices(){
        ArrayList<DeviceBluetooth> s = new ArrayList<DeviceBluetooth>();
        itemsAdapter = new BluetoothListAdapter(this, s);
        lvBluetoothDevices.setAdapter(itemsAdapter);

    }
    private void loadData(Action action) {
        sEnableDisableBluetooth.setChecked(action.isTurnOnOff());
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:

                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:

                        break;
                    case BluetoothAdapter.STATE_ON:
                        getDataFromActiveBluetooth();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Toast.makeText(getApplicationContext(), "Iniciando...", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        }
    };

    @Override
    protected void onPostResume() {
        super.onPostResume();
        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mReceiver);
    }
}
