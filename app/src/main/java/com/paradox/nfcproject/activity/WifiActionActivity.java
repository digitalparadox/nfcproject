package com.paradox.nfcproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.component.LabelledSpinner;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.sql.QueryUtils;
import com.paradox.nfcproject.util.KeyMgmt;

import java.util.ArrayList;
import java.util.Collections;

public class WifiActionActivity extends AppCompatActivity implements LabelledSpinner.OnItemChosenListener {

    private Toolbar toolbar;
    private FloatingActionButton fabAddTask;

    private SwitchCompat sEnableDisableWIFI;

    private LabelledSpinner lsConnectionType;
    private EditText etNetworkName;
    private EditText etWifiPassword;


    private LinearLayout llContainerWifi;

    private TextInputLayout tilNetworkName, tilNetworkPassword;


    private boolean enableDisablewifi = false;
    Action action;

    int taskId = 0;
    int actionId = 0;

    int encryptationType = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_action);


        initComponents();
        enableEditText(false);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskId = extras.getInt("task_id");
            actionId = extras.getInt("action_id", 0);
            if (actionId > 0) {
                action = QueryUtils.getActionById(getApplicationContext(), actionId);
                if (action != null) {
                    loadData();
                }
            }


        }


    }


    private void initComponents() {

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setElevation(0);

        getSupportActionBar().setTitle("");

        sEnableDisableWIFI = (SwitchCompat) findViewById(R.id.sEnableDisableWIFI);
        llContainerWifi = (LinearLayout) findViewById(R.id.llContainerWifi);


        tilNetworkName = (TextInputLayout) findViewById(R.id.tilNetworkName);
        tilNetworkPassword = (TextInputLayout) findViewById(R.id.tilNetworkPassword);

        fabAddTask = (FloatingActionButton) findViewById(R.id.fabAddActionWifi);
        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveActionData();
            }
        });

        sEnableDisableWIFI.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    enableDisablewifi = true;
                    enableEditText(true);
                } else {
                    enableDisablewifi = false;
                    enableEditText(false);


                    lsConnectionType.setSelection(0);
                    etNetworkName.setText("");
                    etWifiPassword.setText("");


                }

            }
        });

        etNetworkName = (EditText) findViewById(R.id.etNetworkName);
        etWifiPassword = (EditText) findViewById(R.id.etWifiPassword);


        lsConnectionType = (LabelledSpinner) findViewById(R.id.lsConnectionType);
        ArrayList<String> keys = new ArrayList<String>();
        Collections.addAll(keys, KeyMgmt.strings);
        lsConnectionType.setItemsArray(keys);

        lsConnectionType.setOnItemChosenListener(this);


    }

    private void enableEditText(boolean isChecked) {
        if (isChecked) {
            llContainerWifi.requestFocus();
            lsConnectionType.setEnabled(true);
            etNetworkName.setEnabled(true);
            etWifiPassword.setEnabled(true);
            tilNetworkPassword.refreshDrawableState();
            tilNetworkName.refreshDrawableState();
        } else {
            llContainerWifi.requestFocus();
            lsConnectionType.setEnabled(false);
            etNetworkName.setEnabled(false);
            etWifiPassword.setEnabled(false);
            tilNetworkPassword.refreshDrawableState();
            tilNetworkName.refreshDrawableState();


        }
    }

    private void saveActionData() {

        if(action == null)
           action = new Action();


        action.setTaskId(taskId);
        action.setType(App.TYPE_WIFI);
        action.setAction("WIFI");
        action.setDeviceName(etNetworkName.getText().toString());
        action.setPassword(etWifiPassword.getText().toString());

        action.setWifiTypeEncrypt(encryptationType);
        action.setTurnOnOff(enableDisablewifi);


        long id = 0;
        if (actionId > 0) {
            QueryUtils.updateAction(getApplicationContext(), action);
        } else {
            id = QueryUtils.saveNewAction(getApplicationContext(), action);
        }


        Intent i = getIntent();
        i.putExtra("RESULT", id);
        setResult(RESULT_OK, i);
        finish();

    }

    private void loadData() {
        sEnableDisableWIFI.setChecked(action.isTurnOnOff());
        int tyepEn = action.getWifiTypeEncrypt();
        lsConnectionType.setSelection(tyepEn);
        etNetworkName.setText(action.getDeviceName());
        etWifiPassword.setText(action.getPassword());
        if (action.isTurnOnOff()) {
            enableEditText(true);
        }else{
            enableEditText(false);
        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
        String selectedText = adapterView.getItemAtPosition(position).toString();
        switch (labelledSpinner.getId()) {
            case R.id.lsConnectionType:
                encryptationType = position;
                break;
            // If you have multiple LabelledSpinners, you can add more cases here
        }
    }

    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

    }
}
