package com.paradox.nfcproject.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Window;
import android.widget.Toast;

import com.paradox.nfcproject.R;
import com.paradox.nfcproject.service.NFCIntentService;

public class TagActionActivity extends Activity {

    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    boolean writeMode;
    NfcAdapter adapter;
    Tag myTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tag_action);
        initNFC();

        Intent intent = getIntent();

      /*  Toast.makeText(this, "INTENT "+intent
                , Toast.LENGTH_LONG).show();//myTag.toString()

*/
        String action = intent.getAction();

        String type2 = intent.getType();
        //  if (intent.getType() != null && intent.getType().equals("application/" + getPackageName())) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction()) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

           // Toast.makeText(this, "Action DISCOVERED", Toast.LENGTH_LONG).show();//myTag.toString()


            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            //execute
            if(rawMsgs!=null && rawMsgs.length>0){
                NdefRecord relayRecord = ((NdefMessage)rawMsgs[0]).getRecords()[0];
                String nfcData = new String(relayRecord.getPayload());
                if (nfcData != null) {
                    Toast.makeText(this, "TAG Found", Toast.LENGTH_SHORT).show();//myTag.toString()
                    resolveTagString(nfcData);
                }
            }else{
                Toast.makeText(this, "TAG Empty", Toast.LENGTH_SHORT).show();//myTag.toString()

            }



            //   if (MIME_TEXT_PLAIN.equals(type)) {

            //  Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            //    new NdefReaderTask().execute(myTag);


        } else {
            //Log.d("MainActivity", "Wrong mime type: " + type);
        }

    }

    private void initNFC() {
        adapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};
    }

    public void resolveTagString(final String pstring){
        if(!pstring.isEmpty()){

            Intent msgIntent = new Intent(TagActionActivity.this, NFCIntentService.class);
            msgIntent.putExtra("tag_string", pstring);
            startService(msgIntent);


        }else{
            Toast.makeText(this, "TAG Empty", Toast.LENGTH_SHORT).show();//myTag.toString()

        }

        finish();

    }

    /*//en onnewIntent manejamos el intent para encontrar el Tag
    protected void onNewIntent(Intent intent) {

        String action = intent.getAction();

        String type2 = intent.getType();
        //  if (intent.getType() != null && intent.getType().equals("application/" + getPackageName())) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            Toast.makeText(this, "Action ", Toast.LENGTH_LONG).show();//myTag.toString()


            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            //execute
            if(rawMsgs!=null && rawMsgs.length>0){
                NdefRecord relayRecord = ((NdefMessage)rawMsgs[0]).getRecords()[0];
                String nfcData = new String(relayRecord.getPayload());
                if (nfcData != null) {
                    Toast.makeText(this, "TAG Found", Toast.LENGTH_LONG).show();//myTag.toString()

                    resolveTagString(nfcData);
                }
            }



            //   if (MIME_TEXT_PLAIN.equals(type)) {

            //  Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            //    new NdefReaderTask().execute(myTag);


        } else {
            //Log.d("MainActivity", "Wrong mime type: " + type);
        }


        // }
    }
*/

    @Override
    public void onPause(){
        super.onPause();
        WriteModeOff();
    }
    @Override
    public void onResume(){
        super.onResume();
        WriteModeOn();


    }
    private void WriteModeOn(){
        writeMode = true;
        adapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    private void WriteModeOff(){
        writeMode = false;
        adapter.disableForegroundDispatch(this);
    }

}
