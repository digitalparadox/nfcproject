package com.paradox.nfcproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.adapter.ActionsAdapter;
import com.paradox.nfcproject.listener.RecyclerItemClickListener;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.model.Task;
import com.paradox.nfcproject.sql.QueryUtils;

public class TaskActionsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private RecyclerView mRecyclerViewTasks;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    private Task task;
    private EditText etTaskName;

    private FloatingActionsMenu fabAddTaskMenu;
    private FloatingActionButton fabActionButtonWifi, fabActionButtonBluetooth,
                fabActionButtonGPS, fabActionButtonShareCard, fabActionButtonLaunchApp;


    private int NEW_ACTION =2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_actions);

        initComponents();
        int id = 0;

        Bundle bundle = getIntent().getExtras();
        if(bundle !=null){
           id =  bundle.getInt("task_id");
            loadData(id);
        }else{
            loadData();
        }



    }


    private void initComponents() {

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setElevation(0);

        mRecyclerViewTasks = (RecyclerView) findViewById(R.id.rvTaskActions);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerViewTasks.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewTasks.setLayoutManager(mLayoutManager);


        etTaskName = (EditText)findViewById(R.id.etTaskName);
        fabAddTaskMenu = (FloatingActionsMenu)findViewById(R.id.fabAddTaskMenu);

        /*
        fabActionButtonWifi, fabActionButtonBluetooth,
                        fabActionButtonGPS, fabActionButtonShareCard, fabActionButtonLaunchApp
         */
        fabActionButtonWifi = (FloatingActionButton) findViewById(R.id.fabActionButtonWifi);
        fabActionButtonWifi.setOnClickListener(this);
        fabActionButtonWifi.setVisibility(View.GONE);

        fabActionButtonBluetooth = (FloatingActionButton) findViewById(R.id.fabActionButtonBluetooth);
        fabActionButtonBluetooth.setOnClickListener(this);
        fabActionButtonBluetooth.setVisibility(View.GONE);

        fabActionButtonGPS = (FloatingActionButton) findViewById(R.id.fabActionButtonGPS);
        fabActionButtonGPS.setOnClickListener(this);
        fabActionButtonGPS.setVisibility(View.GONE);

        fabActionButtonShareCard = (FloatingActionButton) findViewById(R.id.fabActionButtonShareCard);
        fabActionButtonShareCard.setOnClickListener(this);
        fabActionButtonShareCard.setVisibility(View.GONE);

        fabActionButtonLaunchApp = (FloatingActionButton) findViewById(R.id.fabActionButtonLaunchApp);
        fabActionButtonLaunchApp.setOnClickListener(this);
        fabActionButtonLaunchApp.setVisibility(View.GONE);

        fabAddTaskMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabActionButtonWifi.setVisibility(View.VISIBLE);
                fabActionButtonBluetooth.setVisibility(View.VISIBLE);
                fabActionButtonGPS.setVisibility(View.VISIBLE);
                fabActionButtonShareCard.setVisibility(View.VISIBLE);
                fabActionButtonLaunchApp.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                fabActionButtonWifi.setVisibility(View.GONE);
                fabActionButtonBluetooth.setVisibility(View.GONE);
                fabActionButtonGPS.setVisibility(View.GONE);
                fabActionButtonShareCard.setVisibility(View.GONE);
                fabActionButtonLaunchApp.setVisibility(View.GONE);
            }
        });


        etTaskName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (task != null)
                    task.setName(etTaskName.getText().toString());

                if (task != null && task.getId() > 0) {
                    QueryUtils.updateTask(getApplicationContext(), task);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });


        mRecyclerViewTasks.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // ...
                Action action = task.getActions().get(position);
                int action_id = action.getId();

                Intent intent = null;
                if(action.getType() == App.TYPE_WIFI){
                    intent = new Intent(TaskActionsActivity.this, WifiActionActivity.class);
                }
                if(action.getType() == App.TYPE_BLUETOOTH){
                    intent = new Intent(TaskActionsActivity.this, BluetoothActionActivity.class);
                }
                if(action.getType() == App.TYPE_GPS){
                    intent = new Intent(TaskActionsActivity.this, GPSActionActivity.class);
                }
                if(action.getType() == App.TYPE_LAUNCH_APP){
                    intent = new Intent(TaskActionsActivity.this, ApplicationActionActivity.class);
                }
                intent.putExtra("task_id", task.getId());
                intent.putExtra("action_id", action_id);
                startActivityForResult(intent, NEW_ACTION);
            }

        }));


    }


    private void loadData() {
        
        task = new Task();
        task.setName("New Task");
        long id = QueryUtils.saveNewTask(getApplicationContext(), task);
        task.setId((int)id);
        etTaskName.setText(task.getName());

        mAdapter = new ActionsAdapter(task.getActions());
        mRecyclerViewTasks.setAdapter(mAdapter);

    }
    private void loadData(int taskId) {
        task = QueryUtils.getTaskById(this, taskId);
        etTaskName.setText(task.getName());
        mAdapter = new ActionsAdapter(task.getActions());
        mRecyclerViewTasks.setAdapter(mAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
     //   getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        fabAddTaskMenu.collapse();

        Intent intent = null;
        if(!etTaskName.getText().toString().isEmpty()){

            switch (v.getId()){
                case R.id.fabActionButtonBluetooth:

                    intent = new Intent(TaskActionsActivity.this, BluetoothActionActivity.class);
                    intent.putExtra("task_id", task.getId());
                    startActivityForResult(intent, App.TYPE_BLUETOOTH);
                    break;
                case R.id.fabActionButtonWifi:

                    intent = new Intent(TaskActionsActivity.this, WifiActionActivity.class);
                    intent.putExtra("task_id", task.getId());
                    startActivityForResult(intent, App.TYPE_WIFI);
                    break;
                case R.id.fabActionButtonGPS:

                    intent = new Intent(TaskActionsActivity.this, GPSActionActivity.class);
                    intent.putExtra("task_id", task.getId());
                    startActivityForResult(intent, App.TYPE_GPS);
                    break;
                case R.id.fabActionButtonShareCard:

                     intent = new Intent(TaskActionsActivity.this, ShareCardActionActivity.class);
                    intent.putExtra("task_id", task.getId());
                    startActivityForResult(intent, App.TYPE_SHARE_CARD);
                    break;
                case R.id.fabActionButtonLaunchApp:

                    intent = new Intent(TaskActionsActivity.this, ApplicationActionActivity.class);
                    intent.putExtra("task_id", task.getId());
                    startActivityForResult(intent, App.TYPE_LAUNCH_APP);
                    break;
                default:
                    break;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode ==  App.TYPE_BLUETOOTH
                || requestCode ==  App.TYPE_WIFI
                || requestCode ==  App.TYPE_SHARE_CARD
                || requestCode ==  App.TYPE_GPS ||
                requestCode ==  App.TYPE_LAUNCH_APP) {

            if (resultCode == RESULT_OK) {
                long resultado = data.getExtras().getLong("RESULT");
                loadData(task.getId());
            }
        }
        if(resultCode == RESULT_CANCELED){
            Toast.makeText(getApplicationContext(), "Operación cancelada", Toast.LENGTH_LONG).show();
        }
    }
}
