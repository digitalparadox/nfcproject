package com.paradox.nfcproject.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.adapter.ApplicationsAdapter;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.sql.QueryUtils;

import java.util.ArrayList;
import java.util.List;

public class ApplicationActionActivity extends AppCompatActivity {

    Toolbar toolbar;
    ListView lvApplications;
    ApplicationsAdapter applicationsAdapter;

    private PackageManager packageManager = null;

    private List<ApplicationInfo> applist = null;

    ApplicationInfo applicationInfo = null;
    int pos = 0;

    Action action;

    int taskId = 0;
    int actionId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_action);


        initComponents();
        loadApplications();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskId = extras.getInt("task_id");
            actionId = extras.getInt("action_id", 0);
            if (actionId > 0) {
                action = QueryUtils.getActionById(getApplicationContext(), actionId);
                if (action != null) {
                    loadData(action);
                }
            }


        }


    }

    private void loadData(Action action) {
        // lvApplications.setSelection();
    }

    private void initComponents() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setElevation(0);

        getSupportActionBar().setTitle("");


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAddApplication);
        lvApplications = (ListView) findViewById(R.id.lvApplications);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveActionData();
            }
        });
    }


    private void saveActionData() {
        if (applicationInfo != null) {

            if(action == null)
                action = new Action();


            action.setTaskId(taskId);
            action.setType(App.TYPE_LAUNCH_APP);
            action.setAction("Application");
            action.setApplication(applicationInfo.loadLabel(packageManager) + "");
            action.setExtras(applicationInfo.packageName);


            long id = 0;
            if (actionId > 0) {
                QueryUtils.updateAction(getApplicationContext(), action);
            } else {
                id = QueryUtils.saveNewAction(getApplicationContext(), action);
            }


            Intent i = getIntent();
            i.putExtra("RESULT", id);
            setResult(RESULT_OK, i);
            finish();
        }

    }


    private void loadApplications() {


        packageManager = getPackageManager();

        new LoadApplications().execute();

    }

    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return applist;
    }

    public void setListAdapter(ApplicationsAdapter listAdapter) {
        this.applicationsAdapter = listAdapter;
        lvApplications.setAdapter(applicationsAdapter);


        if(action!=null && !action.getExtras().isEmpty() && applist!=null){

            for (int i = 0; i < applist.size() ; i++) {

                if(applist.get(i).packageName.equalsIgnoreCase(action.getExtras())) {
                    pos = i;
                    break;
                }
            }

        }


        lvApplications.clearFocus();
        lvApplications.post(new Runnable() {
            @Override
            public void run() {
                lvApplications.setSelection(pos);
                lvApplications.setItemChecked(pos, true);

            }
        });



        lvApplications.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(applist!=null)
                    applicationInfo =  applist.get(position);
            }
        });
    }


    private class LoadApplications extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            applist = checkForLaunchIntent(getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA));
            applicationsAdapter = new ApplicationsAdapter(ApplicationActionActivity.this,
                    R.layout.application_item_row, applist);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            setListAdapter(applicationsAdapter);
            progress.dismiss();
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(ApplicationActionActivity.this, null,
                    "Loading application info...");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

}
