package com.paradox.nfcproject.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.paradox.nfcproject.App;
import com.paradox.nfcproject.R;
import com.paradox.nfcproject.model.Action;
import com.paradox.nfcproject.sql.QueryUtils;

public class ShareCardActionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FloatingActionButton fabAddContactCard;

    Action action;

    int taskId = 0;
    int actionId = 0;

    final static int PICK_CONTACT = 1;

    EditText etName, etPhone, etAddress, etEmail;

    ImageButton iBopenContactFromIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharecontact_action);

        initComponents();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskId = extras.getInt("task_id");
            actionId = extras.getInt("action_id", 0);
            if (actionId > 0) {
                action = QueryUtils.getActionById(getApplicationContext(), actionId);
                if (action != null) {
                    loadData(action);
                }
            }


        }


    }


    private void initComponents() {

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setElevation(0);

        getSupportActionBar().setTitle("");


        iBopenContactFromIntent = (ImageButton) findViewById(R.id.iBopenContactFromIntent);

        iBopenContactFromIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 1);


            }
        });


        fabAddContactCard = (FloatingActionButton) findViewById(R.id.fabAddContactCard);
        fabAddContactCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveActionData();
            }
        });


        etName = (EditText) findViewById(R.id.etName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etEmail = (EditText) findViewById(R.id.etEmail);

    }

    private void saveActionData() {

        if (!etName.getText().toString().isEmpty() &&
                !etPhone.getText().toString().isEmpty() &&
                !etAddress.getText().toString().isEmpty() &&
                !etEmail.getText().toString().isEmpty()) {

            if (action == null)
                action = new Action();


           /* VCard vcard = new VCard();


            if (!etName.getText().toString().isEmpty())
                vcard.setFormattedName(etPhone.getText().toString());

            if (!etPhone.getText().toString().isEmpty())
                vcard.addTelephoneNumber(etPhone.getText().toString());

            if (!etAddress.getText().toString().isEmpty()) {
                Address adr = new Address();
                adr.setStreetAddress(etAddress.getText().toString());
                adr.addType(AddressType.HOME);
                vcard.addAddress(adr);

            }

            if (!etEmail.getText().toString().isEmpty())
                vcard.addEmail(etEmail.getText().toString());

            String textCard = Ezvcard.write(vcard).version(VCardVersion.V3_0).go();*/


            action.setTaskId(taskId);
            action.setType(App.TYPE_SHARE_CARD);
            action.setAction("Share Contact");
            action.setTurnOnOff(true);
          //  action.setExtras(textCard);


            long id = 0;
            if (actionId > 0) {
                QueryUtils.updateAction(getApplicationContext(), action);
            } else {
                id = QueryUtils.saveNewAction(getApplicationContext(), action);
            }


            Intent i = getIntent();
            i.putExtra("RESULT", id);
            setResult(RESULT_OK, i);
            finish();
        }

    }

    private void loadData(Action action) {
      /*  VCard vcard = Ezvcard.parse(action.getExtras()).first();

        etName.setText(vcard.getFormattedName().toString());
        etAddress.setText(vcard.getTelephoneNumbers().get(0).toString());
        etPhone.setText(vcard.getAddresses().get(0).getStreetAddress());
        etEmail.setText(vcard.getEmails().get(0).toString());*/
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data){
        super.onActivityResult(reqCode, resultCode, data);

        switch(reqCode)
        {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK)
                {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst())
                    {
                      //  String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
/*
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1"))
                        {
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                            phones.moveToFirst();
                            String cNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            Toast.makeText(getApplicationContext(), cNumber, Toast.LENGTH_SHORT).show();

                            String nameContact = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

                           // editText.setText(nameContact+ " "+ cNumber);
                        }*/
                    }
                }
        }
    }
}
